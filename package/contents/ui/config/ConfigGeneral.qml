import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

import org.kde.kirigami 2.5 as Kirigami
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

import "../lib"
import ".."

ConfigPage {
	id: page
	MobileForm.FormCard {
            Layout.fillWidth: true
			visible: plasmoid.location != PlasmaCore.Types.Floating
            contentItem: ColumnLayout {
                spacing: 0
                MobileForm.FormCardHeader{
                    title: "Panel Icon"
                }
                MobileForm.AbstractFormDelegate {
					background: Item{}
					height: icon.height
					RowLayout{
						id: icon
						width: parent.width
						ConfigIcon {
							Layout.margins: 10
							configKey: 'icon'
						}
					}
                }
            }
	}
	MobileForm.FormCard {
            Layout.fillWidth: true

            contentItem: ColumnLayout {
                spacing: 0
                MobileForm.FormCardHeader{
                    title: "Note Path"
                }
                MobileForm.FormSwitchDelegate {
					id: useGlobalNote
                    text: i18n("Use Global Note")
                    description: i18n("Use one shared Note for every Todo widgets.")
                    checked: plasmoid.configuration["useGlobalNote"]
					onClicked: plasmoid.configuration["useGlobalNote"] = !plasmoid.configuration["useGlobalNote"]
                }

                MobileForm.FormDelegateSeparator {}

                MobileForm.FormTextFieldDelegate{
					readonly property string configValue: "noteFilename" ? plasmoid.configuration["noteFilename"] : ""
					label: "Note Name:"
					visible: !useGlobalNote.checked
					Timer { // throttle
						id: serializeTimer
						interval: 300
						onTriggered: {
							if ("noteFilename") {
								plasmoid.configuration["noteFilename"] = parent.text
							}
						}
					}
					text: configValue
				}
            }
	}
	MobileForm.FormCard {
		Layout.fillWidth: true

		contentItem: ColumnLayout {
			spacing: 0
			MobileForm.FormCardHeader{
				title: "Completed Todos"
			}
			MobileForm.FormSwitchDelegate {
				text: i18n("Delete on complete")
				description: i18n("Delete Todos when they are marked as completed.")
				checked: plasmoid.configuration["deleteOnComplete"]
				onClicked: plasmoid.configuration["deleteOnComplete"] = !plasmoid.configuration["deleteOnComplete"]
			}

			MobileForm.FormDelegateSeparator {}

			MobileForm.FormSwitchDelegate {
				text: i18n("Strikeout")
				description: i18n("Strikeout completet Todos")
				checked: plasmoid.configuration["strikeoutCompleted"]
				onClicked: plasmoid.configuration["strikeoutCompleted"] = !plasmoid.configuration["strikeoutCompleted"]
			}
			MobileForm.FormDelegateSeparator {}

			MobileForm.FormSwitchDelegate {
				text: i18n("Fade")
				description: i18n("Fade out completet Todos")
				checked: plasmoid.configuration["fadeCompleted"]
				onClicked: plasmoid.configuration["fadeCompleted"] = !plasmoid.configuration["fadeCompleted"]
			}
		}
	}
	MobileForm.FormCard {
		visible: plasmoid.location != PlasmaCore.Types.Floating
		Layout.fillWidth: true

		contentItem: ColumnLayout {
			spacing: 0
			MobileForm.FormCardHeader{
				title: "Show Counter"
			}
			MobileForm.FormRadioDelegate {
				text: "Never"
				description: "Never display the Counter"
				checked: plasmoid.configuration["showCounter"] == "Never"
				onClicked: plasmoid.configuration["showCounter"] = "Never"
			}
			MobileForm.FormRadioDelegate {
				text: "Incomplete"
				description: "Only display Counter when Items are marked as Incomplete"
				checked: plasmoid.configuration["showCounter"] == "Incomplete"
				onClicked: plasmoid.configuration["showCounter"] = "Incomplete"
			}
			MobileForm.FormRadioDelegate {
				text: "Always"
				description: "Always display the Counter"
				checked: plasmoid.configuration["showCounter"] == "Always"
				onClicked: plasmoid.configuration["showCounter"] = "Always"
			}
		}
	}
	MobileForm.FormCard {
		visible: plasmoid.location != PlasmaCore.Types.Floating
		Layout.fillWidth: true

		contentItem: ColumnLayout {
			spacing: 0
			MobileForm.FormCardHeader{
				title: "Counter Style"
			}
			MobileForm.FormSwitchDelegate {
				text: i18n("Use big Counter")
				description: i18n("Display the Counter Instead of the Icon.")
				checked: plasmoid.configuration["bigCounter"]
				onClicked: plasmoid.configuration["bigCounter"] = !plasmoid.configuration["bigCounter"]
			}
			MobileForm.FormSwitchDelegate {
				text: i18n("Use round counter")
				description: i18n("Use Rounded Counter insead of Square one.")
				checked: plasmoid.configuration["roundCounter"]
				onClicked: plasmoid.configuration["roundCounter"] = !plasmoid.configuration["roundCounter"]
			}
		}
	}
}
