import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.kirigami 2.5 as Kirigami


PlasmaExtras.Representation {
	id: fullRepresentation
	Layout.minimumWidth: units.gridUnit * 10
	Layout.minimumHeight: units.gridUnit * 10
	Layout.preferredWidth: units.gridUnit * 20
	Layout.preferredHeight: Math.min(Math.max(units.gridUnit * 20, maxContentHeight), Screen.desktopAvailableHeight) // Binding loop warning (meh).
	property int maxContentHeight: 0
	function updateMaxContentHeight() {
		var maxHeight = 0
		for (var i = 0; i < notesRepeater.count; i++) {
			var item = notesRepeater.itemAt(i)
			if (item) {
				if (!item.ready) {
					return // Not ready yet
				}
				maxHeight = Math.max(maxHeight, item.contentHeight)
			}
		}
		// console.log('maxContentHeight', maxHeight)
		maxContentHeight = maxHeight
	}
	// property int contentHeight: pinButton.height + container.spacing + listView.contentHeight
	// Layout.maximumWidth: plasmoid.screenGeometry.width
	// Layout.maximumHeight: plasmoid.screenGeometry.height

	property bool isDesktopContainment: false
	header: PlasmaExtras.PlasmoidHeading {
			leftPadding: PlasmaCore.Units.smallSpacing
			contentItem: RowLayout {
				PlasmaComponents3.TextField {
					id: textField
					Layout.fillWidth: true
					Layout.margins: 0
					text: noteItem.heading
					placeholderText: i18n("Todo List")


					// TODO: Use a Loader to re-implement usingPlasmaStyle
					// readonly property bool usingPlasmaStyle: plasmoid.configuration.listTitlePlasmaStyle
					readonly property bool usingPlasmaStyle: false
					// These properties are set for !usingPlasmaStyle.
					background: Item {}
					font.pixelSize: 25
					font.weight: Font.Bold
					implicitHeight: contentHeight
					padding: 0
					leftPadding: 0
					rightPadding: 0
					topPadding: 0
					bottomPadding: 0
					topInset: 0


					onEditingFinished: {
						noteItem.heading = text
						text = Qt.binding(function() { return noteItem.heading })
					}
				}
				PlasmaComponents3.ToolButton {
					Layout.margins: 0
					icon.name: "list-add"
					height: width
					onClicked: {
						noteItem.saveNote()
						noteItem.updateTodoData()
						noteItem.updateAllModels()
						noteItem.sectionList.push({
							label: '',
							items: [noteItem.newTodoItem()],
						})
						noteItem.numSections++
						noteItem.updateTodoData()
						noteItem.updateAllModels()
					}
				}
				PlasmaComponents3.ToolButton {
					id: pinButton
					Layout.margins: 0
					height: width
					checkable: true
					icon.name: "window-pin"
					onCheckedChanged: plasmoid.hideOnWindowDeactivate = !checked
					visible: !isDesktopContainment
				}
			}
		}


	GridLayout {
		id: notesRow
		anchors.fill: parent
		flow: fullRepresentation.width > fullRepresentation.height? GridLayout.LeftToRight:GridLayout.TopToBottom
		Behavior on opacity {
			NumberAnimation { duration: 400 }
		}

		Repeater {
			id: notesRepeater
			model: noteItem.numSections

			NoteSection {
				id: container


			}
		}
	}
}
